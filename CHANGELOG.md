# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## 0.5.0 - 2018.09.14

### Added
-  Add a label to the service to specify scaling size. 
  [BD-2147](https://opensource.ncsa.illinois.edu/jira/browse/BD-2147)


