import unittest
import mock

import clusterscale
from clusterscale import swarm
from clusterscale.swarmscaling import SwarmScaling


def messages_upper_bound(replica, scale_factor):
    return replica * scale_factor + SwarmScaling.jitter


def messages_lower_bound(replica, scale_factor):
    return max(0, replica * scale_factor - SwarmScaling.jitter)


class SwarmScalingTestCases(unittest.TestCase):
    def setUp(self):
        with mock.patch.object(clusterscale.rabbitmq.Rabbitmq, "__init__", lambda x: None):
            with mock.patch.object(clusterscale.swarm.Swarm, "__init__", lambda x, y: None):
                    self.scale_instance = clusterscale.swarmscaling.SwarmScaling(None)
                    self.rabbitmq = clusterscale.rabbitmq.Rabbitmq()

    def _testcase(self, mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                  num_msg_ready, current_replica, replicas_max, replicas_min):
        with mock.patch.object(clusterscale.swarm.Swarm, "scale_replica", lambda x, y, z: None):
            mocked_rabbitmq_getqueues.return_value = {'key1': ('name1', 200, 1, num_msg_ready)}
            mocked_get_swarm_services_metadata.return_value = \
                [('key1', {
                    'name': 'servicename',
                    'labels': {
                        'bd.level': 'level',
                        'bd.rabbitmq.queue': 'rabbitmq.queue.name',
                        'bd.rabbitmq.vhost': 'vhost',
                        'bd.replicas.max': replicas_max,
                        'bd.replicas.min': replicas_min,
                        'bd.type': 'type'},
                    'replicas': {'requested': current_replica, 'running': '_'}})
                 ]

            (_, scale_data) = self.scale_instance.scale('vhost', self.rabbitmq, None)
            result = scale_data[0]
            return result.replica, result.scale_decision

    # let replicas_min equals replicas_max.
    @mock.patch('clusterscale.swarm.Swarm.get_swarm_services_metadata')
    @mock.patch('clusterscale.rabbitmq.Rabbitmq.get_queues')
    def test_swarmscaling_scale_replicas_maxmin_equals(self, mocked_rabbitmq_getqueues,
                                                       mocked_get_swarm_services_metadata):
        replicas_min = 5
        replicas_max = replicas_min
        num_msg_ready = 100
        current_replica = 10

        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        self.assertEqual(replicas_max, new_replicas)

    # let current_replica bigger than replica_max,
    # and expect to see correct new_replicas when changing num_msg_ready in different boundaries
    @mock.patch('clusterscale.swarm.Swarm.get_swarm_services_metadata')
    @mock.patch('clusterscale.rabbitmq.Rabbitmq.get_queues')
    def test_swarmscaling_scale_current_replica_biggerthan_replica_max(self, mocked_rabbitmq_getqueues,
                                                                       mocked_get_swarm_services_metadata):
        replicas_min = 0
        replicas_max = 10
        current_replica = replicas_max + 1
        scale_factor = SwarmScaling.sizing
        messages_upperbound = clusterscale.swarmscaling.SwarmScaling.messages_upper_bound(current_replica, scale_factor)
        messages_lowerbound = clusterscale.swarmscaling.SwarmScaling.messages_lower_bound(current_replica, scale_factor)

        # num_msg_ready larger than messages_upperbound
        num_msg_ready = messages_upperbound + 1

        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect new_replicas equals replicas_max
        self.assertEqual(replicas_max, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.SCALE_DOWN, scale_decision)

        # num_msg_ready in (messages_lowerbound, messages_upperbound)
        num_msg_ready = messages_lowerbound + 1
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect new_replicas equals replicas_max
        self.assertEqual(replicas_max, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.SCALE_DOWN, scale_decision)

        # num_msg_ready in [0, messages_lowerbound)
        num_msg_ready = messages_lowerbound - 1
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect new_replicas equals replicas_max
        self.assertEqual(replicas_max, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.SCALE_DOWN, scale_decision)

        # num_msg_ready equals to messages_upperbound
        num_msg_ready = messages_upperbound
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect new_replicas equals replicas_max
        self.assertEqual(replicas_max, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.SCALE_DOWN, scale_decision)

        # num_msg_ready equals to messages_lowerbound
        num_msg_ready = messages_lowerbound
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect new_replicas equals replicas_max
        self.assertEqual(replicas_max, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.SCALE_DOWN, scale_decision)

    # let current_replica smaller than replica_max and bigger than replica_min
    # and expect to see correct new_replicas when changing num_msg_ready in different boundaries
    @mock.patch('clusterscale.swarm.Swarm.get_swarm_services_metadata')
    @mock.patch('clusterscale.rabbitmq.Rabbitmq.get_queues')
    def test_swarmscaling_scale_current_replica_smallerthan_replica_max_biggerthan_replica_min(
            self, mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata):
        replicas_min = 0
        replicas_max = 10
        current_replica = 4
        scale_factor = SwarmScaling.sizing
        messages_upperbound = clusterscale.swarmscaling.SwarmScaling.messages_upper_bound(current_replica, scale_factor)
        messages_lowerbound = clusterscale.swarmscaling.SwarmScaling.messages_lower_bound(current_replica, scale_factor)
        # num_msg_ready (messages_upperbound, )

        num_msg_ready = messages_upperbound + 1
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect new_replicas increase by 1 and scale up
        self.assertEqual(current_replica+1, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.SCALE_UP, scale_decision)

        # num_msg_ready [0, messages_lowerbound)
        num_msg_ready = messages_lowerbound - 1
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect new_replicas deincrease by 1 and scale down
        self.assertEqual(current_replica-1, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.SCALE_DOWN, scale_decision)

        # num_msg_ready in (messages_lowerbound, messages_upperbound)
        num_msg_ready = messages_lowerbound + 1
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect no scale
        self.assertEqual(current_replica, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.NO_SCALE, scale_decision)

        num_msg_ready = messages_lowerbound
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect no scale
        self.assertEqual(current_replica, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.NO_SCALE, scale_decision)

        num_msg_ready = messages_upperbound
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect no scale
        self.assertEqual(current_replica, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.NO_SCALE, scale_decision)

    # let current_replica smaller than replica_min
    # and expect to see correct new_replicas when changing num_msg_ready in different boundaries
    @mock.patch('clusterscale.swarm.Swarm.get_swarm_services_metadata')
    @mock.patch('clusterscale.rabbitmq.Rabbitmq.get_queues')
    def test_swarmscaling_scale_current_replica_smallerthan_replica_min(
            self, mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata):
        replicas_min = 1
        replicas_max = 10
        current_replica = 0
        scale_factor = SwarmScaling.sizing
        messages_upperbound = clusterscale.swarmscaling.SwarmScaling.messages_upper_bound(current_replica, scale_factor)
        messages_lowerbound = clusterscale.swarmscaling.SwarmScaling.messages_lower_bound(current_replica, scale_factor)

        # num_msg_ready (messages_upperbound, )
        num_msg_ready = messages_upperbound + 1
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect new_replicas equals to replicas_min and scale up
        self.assertEqual(replicas_min, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.SCALE_UP, scale_decision)

        # num_msg_ready [0, messages_lowerbound)
        num_msg_ready = messages_lowerbound - 1
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect new_replicas equals to replicas_min and scale up
        self.assertEqual(replicas_min, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.SCALE_UP, scale_decision)

        # num_msg_ready in (messages_lowerbound, messages_upperbound)
        num_msg_ready = messages_lowerbound + 1
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect new_replicas equals to replicas_min and scale up
        self.assertEqual(replicas_min, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.SCALE_UP, scale_decision)

        num_msg_ready = messages_lowerbound
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect new_replicas equals to replicas_min and scale up
        self.assertEqual(replicas_min, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.SCALE_UP, scale_decision)

        num_msg_ready = messages_upperbound
        (new_replicas, scale_decision) = \
            self._testcase(mocked_rabbitmq_getqueues, mocked_get_swarm_services_metadata,
                           num_msg_ready, current_replica, replicas_max, replicas_min)
        # expect new_replicas equals to replicas_min and scale up
        self.assertEqual(replicas_min, new_replicas)
        self.assertEqual(clusterscale.swarmscaling.SwarmScaling.SCALE_UP, scale_decision)

if __name__ == '__main__':
    unittest.main()
