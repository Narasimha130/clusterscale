import unittest
import requests
import mock
from requests.models import Response

import clusterscale
from clusterscale import bdswarm
from clusterscale import swarm


def mock_response_labels_with_queuename():
    return {'shortid1': {
        'labels': {
            'bd.level': 'level',
            'bd.rabbitmq.queue': 'rabbitmq.queue.name',
            'bd.rabbitmq.vhost': 'vhost',
            'bd.replicas.max': '5',
            'bd.replicas.min': '1',
            'bd.type': 'type'}}}


def mock_response_labels_without_queuename():
    return {'shortid1': {
        'labels': {
            'bd.level': 'level',
            'bd.rabbitmq.vhost': 'vhost',
            'bd.replicas.max': '5',
            'bd.replicas.min': '1',
            'bd.type': 'type'}}}


def mocked_request_get_malformed_labels(*args, **kwargs):
    with mock.patch.object(requests.models.Response, "__init__", lambda x: None):
        response = Response()
        response.json = mock_response_labels_without_queuename
        response.status_code = 200
        response.reason = "success"
        return response


def mocked_request_get_perfect_labels(*args, **kwargs):
    with mock.patch.object(requests.models.Response, "__init__", lambda x: None):
        response = Response()
        response.json = mock_response_labels_with_queuename
        response.status_code = 200
        response.reason = "success"
        return response


class SwarmTestCases(unittest.TestCase):
    def setUp(self):
        self.swarmurl = 'http://username:passwd@swarmhost'
        clusterscale.bdswarm.BDSwarms.swarmprefix_vhost_mapping = {'prefix': 'vhost'}

    @mock.patch('requests.get', side_effect=mocked_request_get_perfect_labels)
    def test_bdswarm_services_snapshot_success(self, mock_request_get):
        swarm_instance = swarm.Swarm(self.swarmurl)
        swarm_instance.services_snapshot()
        self.assertTrue(any(service_metadata_pair[0] == 'rabbitmqqueuename' for service_metadata_pair
                            in swarm_instance.get_swarm_services_metadata('vhost')))

    @mock.patch('requests.get', side_effect=mocked_request_get_malformed_labels)
    def test_bdswarm_services_snapshot_failure(self, mock_request_get):
        swarm_instance = swarm.Swarm(self.swarmurl)
        swarm_instance.services_snapshot()
        self.assertIsNone(swarm_instance.get_swarm_services_metadata('vhost'))


if __name__ == '__main__':
    unittest.main()
