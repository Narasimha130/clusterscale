FROM python:3-onbuild
MAINTAINER Bing Zhang <bing@illinois.edu>

ARG BUILD_VERSION

ENV LOGGER="" \
    SWARM_URL="" \
    MONGO_URL="" \
    Rabbitmq_URLS="" \
    BUILD=${BUILD_VERSION:-unknown}

CMD [ "python", "./server.py"]
