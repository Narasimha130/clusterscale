### BD ClusterScale

ClusterScale automatically scales(up/down) replica of services in swarm cluster.
ClusterScale component periodically snapshots the swarm services and monitors requests in rabbitmq queue. If there are more queueing messages for conversion/extraction services in rabbitmq, clusterscale scales up services by putting more replica of services. And meanwhile clusterscale will scale down services if there
are idle services with less requests in rabbitmq queue.

## Development
To run a `clusterscale` locally as a developer you need to have docker installed on your
workstation.

Conveniently, docker comes with swarm installed. You need to enable swarm'ing with the
command `docker swarm init`

Then you run clusterscale as

```commandline
python3 server.py --swarm_url "http://username:passwd@yourhostip:port" --rabbitmq_urls "{'swarm-prefix1' : 'amqp://username:passwd@host:port/vhost1', 'swarm-prefix2' : 'amqp://username:passwd@host:port/vhost2'}"
```

To run clusterscale container:
```
docker run --rm -it --publish 7777:7777 -e 'TZ=/usr/share/zoneinfo/US/Central' -e "MONGO_URL=mongodb://mongohostname:port" -e "SWARM_URL=http://username:passwd@swarmhostname:port" -e "Rabbitmq_URLS={'swarm-prefix1' : 'amqp://username:passwd@host:port/vhost1', 'swarm-prefix2' : 'amqp://username:passwd@host:port/vhost2'}" --name clusterscale clusterscale/clusterscale
```

ClusterScale stores history scaling recorders into mongodb and offers REST API to query.

sample tuple:
```
vhost	    | service_name 	          | queue_name 	  | timestamp 	                  | decision | replica | message_ready | consumer
clowder-dev | dev-extractor-ncsa_cv_faces | ncsa.cv.faces | Wed, 30 Aug 2017 15:38:36 GMT | scale up | 2       | 100	       | 1
```

Simple Query all `scale up' services whose are running on `dap-dev' vhost and using `siegfried' as queue_name in latest 100 recorders:
```
api/events?last=100&decision=scale%20up&queue_name=siegfried&vhost=dap-dev
```

## Unit test
```
python3 -m pytest
```