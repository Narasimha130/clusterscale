import ast
import json
import argparse
import sys
import os.path
import flask
import logging

from clusterscale import bdswarm

app = flask.Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

software_version = '1.0'
columns = ['SwarmServiceName', 'RabbitmqQueueName', 'TimeStamp', 'Consumers', 'Replica', 'Ready Messages', 'Scale']
bdswarm_instance = None


@app.route('/')
def index():
    return flask.render_template('index.html', columns=columns)


def output_result(result_data):
    output = {}
    data_rows = []
    for row in result_data:
        data_row = []
        for i in range(len(columns)):
            data_row.append(str(row[columns[i]]).replace('"', '\\"'))
        data_rows.append(data_row)
    output['data'] = data_rows
    return output


def get_view_data(view_db):
    view_data = view_db.find()
    collection = []
    for datum in view_data:
        swarm_service_name = datum['service_name']
        rabbitmq_queue_name = datum['queue_name']
        timestamp = datum['timestamp']
        consumers = datum['consumers']
        replica = datum['replica']
        message_ready = datum['message_ready']
        scale_decision = datum['decision']
        column_datum = [swarm_service_name, rabbitmq_queue_name, timestamp, consumers, replica, message_ready,
                        scale_decision]
        collection.append(dict(zip(columns, column_datum)))
    return collection


@app.route('/list')
def list_datatable():
    collection = []
    view_dbs = bdswarm_instance.get_viewdbs()
    for tablename, view_db in view_dbs.items():
        view_data = get_view_data(view_db)
        if 0 != len(view_data):
            collection.extend(view_data)

    results = output_result(collection)
    # return the results as a string for the datatable
    return json.dumps(results)


# ----------------------------------------------------------------------
# HELPER FUNCTIONS
# ----------------------------------------------------------------------
def config_logging(config_info):
    if config_info:
        if os.path.isfile(config_info):
            if config_info.endswith('.json'):
                with open(config_info, 'r') as f:
                    config = json.load(f)
                    logging.config.dictConfig(config)
            else:
                logging.config.fileConfig(config_info)
        else:
            config = json.load(config_info)
            logging.config.dictConfig(config)
    else:
        logging.basicConfig(format='%(asctime)-15s %(levelname)-7s [%(threadName)-10s] : %(name)s - %(message)s',
                            level=logging.INFO)
        logging.getLogger(__name__).setLevel(logging.DEBUG)
        logging.getLogger('werkzeug').setLevel(logging.WARNING)
        logging.getLogger('swarm').setLevel(logging.DEBUG)


def main():
    global bdswarm_instance

    # parse command line arguments
    parser = argparse.ArgumentParser(description='Swarm Scaling Parameters')
    parser.add_argument('--logging', '-l', default=os.getenv("LOGGER", None),
                        help='file or logging coonfiguration (default=None)')
    parser.add_argument('--swarm_url', '-s', default=os.getenv("SWARM_URL", None),
                        help='swarm url with username :passwd')
    parser.add_argument('--rabbitmq_urls', '-r', default=os.getenv("Rabbitmq_URLS", None),
                        help='rabbitmq urls as json string')
    parser.add_argument('--mongo_url', '-m', default=os.getenv("MONGO_URL", None),
                        help='mongo url string')
    parser.add_argument('--version', action='version', version='%(prog)s version=' + software_version)
    args = parser.parse_args()

    # setup logging
    config_logging(args.logging)
    logger = logging.getLogger(__name__)

    if not args.logging:
        logging.basicConfig(format='%(asctime)-15s %(levelname)-7s [%(threadName)-10s] : %(name)s - %(message)s',
                            level=logging.INFO)
    else:
        logging.basicConfig(format='%(asctime)-15s %(levelname)-7s [%(threadName)-10s] : %(name)s - %(message)s',
                            level=logging.DEBUG)

    if not args.swarm_url:
        logger.error("input of swarm_url empty")
        parser.print_help()
        sys.exit(-1)

    if not args.rabbitmq_urls:
        logger.error("input of rabbitmqs syntax is wrong")
        parser.print_help()
        sys.exit(-1)

    mongo_url = "mongodb://127.0.0.1:27017"
    if args.mongo_url:
        mongo_url = args.mongo_url

    bdswarm_instance = bdswarm.BDSwarms(app, ast.literal_eval(args.rabbitmq_urls), args.swarm_url, mongo_url)
    bdswarm_instance.start()

    app.run(host='0.0.0.0', port=7777, debug=True)

# ----------------------------------------------------------------------
# MAIN
# ----------------------------------------------------------------------
if __name__ == '__main__':
    main()
