import logging

from clusterscale import db
from clusterscale import swarm


class SwarmScaling:

    sizing = 50
    jitter = 2
    NO_SCALE = '--'
    SCALE_DOWN = 'scale down'
    SCALE_UP = 'scale up'

    def __init__(self, swarm_url):
        self.__swarm = swarm.Swarm(swarm_url)
        self.__logger = logging.getLogger("clusterscale")

    def snapshot(self):
        self.__swarm.services_snapshot()

    @staticmethod
    def messages_upper_bound(replica, scale_factor):
        return replica * scale_factor + SwarmScaling.jitter

    @staticmethod
    def messages_lower_bound(replica, scale_factor):
        return max(0, replica * scale_factor - SwarmScaling.jitter)

    def __do_scale(self, key, service_metadata, queues_mapping):
        swarm_service_name = service_metadata['name']
        replica = service_metadata['replicas']['requested']
        new_replica = replica

        if key in queues_mapping:
            queue_info = queues_mapping[key]
        else:
            print(key, ' not found in rabbitmq!')
            scale_decision = 'unknown'
            messages = 'unknown'
            consumers = 'unknown'
            msgs_ready = 'unknown'
            new_replica = 'unknown'
            return False, swarm_service_name, 'unknown', consumers, msgs_ready, new_replica, scale_decision

        (rabbitmq_queuename, messages, consumers, msgs_ready) = queue_info
        scale_decision = self.NO_SCALE
        scale_factor = service_metadata['labels'].get('bd.replicas.scale', SwarmScaling.sizing)

        try:
            scale_min = service_metadata['labels']['bd.replicas.min']
            scale_max = service_metadata['labels']['bd.replicas.max']
            if type(scale_min) is str:
                scale_min = int(scale_min)
            if type(scale_max) is str:
                scale_max = int(scale_max)
        except Exception as err:
            scale_decision = '-- : invalidate min/max'
            self.__logger.debug('{:>20} {:>12} {:>12} {:>12} {:>12} {:>12}'.
                                format(swarm_service_name, messages, consumers, msgs_ready, scale_decision,
                                       str(replica) + ' to ' + str(new_replica)))
            self.__logger.exception(err)
            return False, swarm_service_name, rabbitmq_queuename, consumers, msgs_ready, new_replica, scale_decision

        # new replica must be in [scale_min, scale_max]
        # no scale if new replica == replica
        # if msgs in rabbitmq larger/smaller enough, will scale new replica gradually +1/-1
        if scale_min == scale_max:
            # check if bd.replicas.min == bd.replicas.max(both can be 0)
            new_replica = scale_min
            if new_replica == replica:
                scale_decision = self.NO_SCALE
            elif new_replica < replica:
                scale_decision = self.SCALE_DOWN
            else:
                scale_decision = self.SCALE_UP
        elif msgs_ready > self.messages_upper_bound(replica, scale_factor) and scale_max > replica:
            # too many msgs in rabbitmq but with not too many number of replica,
            # then scale up by 1 and at least scale_min
            new_replica = max(scale_min, replica + 1)
            scale_decision = self.SCALE_UP
        elif msgs_ready < self.messages_lower_bound(replica, scale_factor) and scale_min < replica:
            # too few msgs in rabbitmq but with not too few number of replica,
            # then scale down by 1 and at most scale_max
            new_replica = min(scale_max, replica - 1)
            scale_decision = self.SCALE_DOWN
        elif replica > scale_max:
            # too many replica but with not too few msgs in rabbitmq, then scale down to scale_max
            new_replica = scale_max
            scale_decision = self.SCALE_DOWN
        elif replica < scale_min:
            # too few replica but with not too many msgs in rabbitmq, then scale up to scale_min
            new_replica = scale_min
            scale_decision = self.SCALE_UP
        else:
            # neither scale up or scale down
            scale_decision = self.NO_SCALE

        self.__logger.debug('{:>20} {:>12} {:>12} {:>12} {:>12} {:>12}'.
                            format(swarm_service_name, messages, consumers, msgs_ready, scale_decision,
                                   str(replica) + ' to ' + str(new_replica)))

        if scale_decision != self.NO_SCALE:
            self.__swarm.scale_replica(swarm_service_name, new_replica)

        return \
            scale_decision != self.NO_SCALE, swarm_service_name, rabbitmq_queuename, consumers, msgs_ready, \
            new_replica, scale_decision

    def scale(self, vhost, rabbitmq, timestamp):
        """
        :param vhost: name of vhost
        :param rabbitmq: instance of Rabbitmq
        :param timestamp: timestamp string
        :return: infotmation mappings between services on docker swarm and rabbitmq
        """
        swarm_mapping = self.__swarm.get_swarm_services_metadata(vhost)
        queues_mapping = rabbitmq.get_queues()

        self.__logger.debug('\n\n\t\t\t\t\t+++ ' + str(timestamp) + ': ' + vhost + ' +++')
        self.__logger.debug('{:>20} {:>12} {:>12} {:>12} {:>12} {:>12}'.format('queuename', 'messages', 'consumers',
                                                                               'msgs_ready', 'decision', 'replica'))

        scale_updates_data = []
        view_data = []
        if swarm_mapping is not None and queues_mapping is not None:
            for service_metadata_pair in swarm_mapping:
                try:
                    (key, swarm_metdata) = service_metadata_pair
                    (scale_update, swarm_service_name, queue_name, consumers, msgs_ready,
                     new_replica, scale_decision) = \
                        self.__do_scale(key, swarm_metdata, queues_mapping)
                    metadata = db.SwarmScaleMetadataSimple(vhost, swarm_service_name, queue_name, timestamp,
                                                           scale_decision, new_replica, msgs_ready, consumers)
                    view_data.append(metadata)
                    if scale_update:
                        scale_updates_data.append(metadata)
                except Exception as ex:
                    # catch exception on single service scales.
                    self.__logger.exception(ex)

        return scale_updates_data, view_data
