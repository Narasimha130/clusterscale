
import requests
import logging
from urllib.parse import urlparse


class Rabbitmq:

    def __init__(self, rabbitmq_url):
        self.__logger = logging.getLogger("clusterscale")
        parsed = urlparse(rabbitmq_url)
        self.__auth = (parsed.username, parsed.password)
        self.__url = 'http://' + parsed.hostname + ':' + str(parsed.port) + '/api/queues' + parsed.path

    def get_queues(self):
        """
        Get info of all managed queues in RabbitMQ.
        Returns:
            A map from queue name to a tuple: (name, num_messages, num_consumers, num_msg_ready).
        """
        bd_qs = {}
        r = requests.get(self.__url, auth=self.__auth)
        r.raise_for_status()
        data = r.json()

        for x in data:
            # skip the 'amq.gen-*' queues that Medici uses to receive extraction status.
            num_msgs_published = 0
            num_msgs_delivered = 0
            # print x.name

            if 'name' in x and not x['name'].startswith('error') and not x['name'].startswith('amq'):
                if 'consumers' in x and 'messages_ready' in x:
                    key = x['name'].replace('.', '')
                    key = key.replace('_', '')
                    key = key.replace('-', '')
                    bd_qs[key] = (x['name'], x['messages'], x['consumers'], x['messages_ready'])
                else:
                    self.__logger.warning(x['name'], " not have consumers or messages_ready")
        return bd_qs
