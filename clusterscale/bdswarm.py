
import logging
import time
import dateutil.tz
from datetime import datetime, timedelta
import threading
from urllib.parse import urlparse

from clusterscale.db import DB
from clusterscale.rabbitmq import Rabbitmq
from clusterscale.api_module import api_bp
from clusterscale.swarmscaling import SwarmScaling


class BDSwarms:
    CHECKING_INTERVAL = 30  # 30 sec
    swarmprefix_vhost_mapping = {}

    def __init__(self, app, rabbitmq_urls, swarm_url, mongo_url):
        self.__logger = logging.getLogger("clusterscale")
        self.__mongo_url = mongo_url
        self.__swarm_scaling_instance = SwarmScaling(swarm_url)
        self.__rabbitmqs = {}
        self.__scale_dbs = {}
        self.__view_dbs = {}

        api_bp.setup_scale_dbs(DB.SCALE_DB_NAME, "scale_tbl", self.__mongo_url)
        for swarm_prefix, rabbitmqurl in rabbitmq_urls.items():
            parsed = urlparse(rabbitmqurl)
            vhost = parsed.path.replace("/", "")
            BDSwarms.swarmprefix_vhost_mapping[swarm_prefix] = vhost
            self.__rabbitmqs.setdefault(vhost, Rabbitmq(rabbitmqurl))
            self.__scale_dbs.setdefault(vhost, DB(DB.SCALE_DB_NAME, "scale_tbl", self.__mongo_url))
            self.__view_dbs.setdefault(vhost, DB(DB.VIEW_DB_NAME, vhost, self.__mongo_url))

        app.register_blueprint(api_bp, url_prefix='/api')

    def get_viewdbs(self):
        return self.__view_dbs

    def __periodically_scaling(self):

        while True:
            try:
                interval = timedelta(seconds=BDSwarms.CHECKING_INTERVAL)
                target_time = datetime.now() + interval

                # execution
                timestamp = datetime.utcnow().replace(tzinfo=dateutil.tz.tzutc())

                self.__swarm_scaling_instance.snapshot()

                for vhost, rabbitmq in self.__rabbitmqs.items():
                    (scale_updates_data, view_data) = self.__swarm_scaling_instance.scale(vhost, rabbitmq, timestamp)
                    # persist in db storage
                    if 0 != len(scale_updates_data):
                        scale_db = self.__scale_dbs.get(vhost)
                        scale_db.update_many(scale_updates_data)

                    if 0 != len(view_data):
                        view_db = self.__view_dbs.get(vhost)
                        view_db.update_many(view_data)

                # Wait for the checking interval, only if this iteration takes shorter than it.
                now = datetime.now()
                if now < target_time:
                    to_sleep = (target_time - now).seconds
                    logging.info("Waiting for " + str(to_sleep) + " seconds...")
                    time.sleep(to_sleep)

            except KeyboardInterrupt:
                self.__logger.info('\nGoodbye! Have a good day!')
                break
            except Exception as ex:
                self.__logger.exception(ex)

    def start(self):
        scale_thread = threading.Thread(name="Periodically scaling", target=self.__periodically_scaling)
        scale_thread.daemon = True
        scale_thread.start()
        return scale_thread
