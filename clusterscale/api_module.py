import flask
from clusterscale.db import DB
from flask import request


class MyAPIBluePrint(flask.Blueprint):
    def __init__(self, name, import_name):
        super(MyAPIBluePrint, self).__init__(self, name, import_name)
        self.scale_recorders_db = None

    def setup_scale_dbs(self, db_name, tbl_name, mongo_url):
        self.scale_recorders_db = DB(db_name, tbl_name, mongo_url)


api_bp = MyAPIBluePrint('api', __name__)


@api_bp.route('/')
def index():
    return 'this is /api'


@api_bp.route('/events', methods=["GET"])
def events():
    json_data = {}
    query_dic = request.args.to_dict()
    result = api_bp.scale_recorders_db.query(query_dic)
    if 0 != len(result):
        json_data = result
    return flask.jsonify(json_data)
