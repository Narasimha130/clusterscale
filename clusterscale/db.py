import logging
import pymongo


class SwarmScaleMetadataSimple:

    def __init__(self, vhost, service_name, queue_name, time_stamp, scale_decision, replica, message_ready, consumers):
        self.vhost = vhost
        self.service_name = service_name
        self.queue_name = queue_name
        self.time_stamp = time_stamp
        self.scale_decision = scale_decision
        self.replica = replica
        self.message_ready = message_ready
        self.consumers = consumers

    def json(self):
        return {
            'vhost': self.vhost,
            'service_name': self.service_name,
            'queue_name': self.queue_name,
            'timestamp': self.time_stamp,
            'decision': self.scale_decision,
            'replica': self.replica,
            'message_ready': self.message_ready,
            'consumers': self.consumers
        }


class DB:
    SCALE_DB_NAME = 'swarmscaling'
    VIEW_DB_NAME = 'viewdb'
    FILTER_ID = {'_id': 0}
    DESCENDING_SORT = [('_id', -1)]

    def __init__(self, database_name, tablename, url):
        client = pymongo.MongoClient(url)
        self.db = client[database_name]
        self.db.collection = self.db[tablename]
        self.__logger = logging.getLogger("clusterscale")

    def update_many(self, data_tuples):
        bulk = self.db.collection.initialize_unordered_bulk_op()
        for datum in data_tuples:
            bulk.find({'service_name': datum.service_name}).upsert().replace_one(datum.json())
        try:
            bulk.execute()
        except pymongo.errors.BulkWriteError as bwe:
            self.__logger.error(bwe.details)

    def find(self):
        """
        :return: list of metadata
        """
        ret = []
        try:
            for data_tuple in self.db.collection.find({}, {'_id': 0}):
                ret.append(data_tuple)
        except pymongo.errors.ServerSelectionTimeoutError as err:
            self.__logger.error(err)
        return ret

    def find_swarm_service_name(self, service_name):
        ret = []
        try:
            for data_tuple in self.db.collection.find({'service_name': service_name}, DB.FILTER_ID):
                ret.append(data_tuple)
        except pymongo.errors.ServerSelectionTimeoutError as err:
            self.__logger.error(err)
        return ret

    def find_one(self, query):
        """
        :param query
        :return: type of dict
        """
        try:
            if query is None:
                return self.db.collection.find_one()
            else:
                return self.db.collection.find_one(query)
        except pymongo.errors.ServerSelectionTimeoutError as err:
            self.__logger.error(err)

    def query(self, query_dic):
        """
        :param query_dic:
        :return:
        """
        ret = []
        recorders = 0
        try:
            recorders = int(query_dic.pop('last', 0))
            if recorders < 0:
                raise ValueError('non positive integer')

            for data_tuple in self.db.collection.find(query_dic, DB.FILTER_ID).limit(recorders).\
                    sort(DB.DESCENDING_SORT):
                ret.append(data_tuple)
        except Exception as err:
            self.__logger.error(err)
            return []
        return ret
